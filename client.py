#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

try:

    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    registro = str(sys.argv[3])
    email = str(sys.argv[4])
    caducidad = int(sys.argv[5])
    direccion = registro + " SIP: " + email + " SIP/2.0" "\r\n" + "Caducidad:" + str(caducidad) + "\r\n"

except IndexError:
    sys.exit("client.py ip puerto register correo tiempo de caducidad")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", direccion)
    my_socket.send(bytes(direccion, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
