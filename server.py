#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}

    def register2json(self):

        with open('registered.json', 'w') as fichero_json:
            json.dump(self.dicc, fichero_json, indent=4)
            print("Usuarios registrados")

    def json2register(self):

        try:
            with open("registered.json", "r") as jsonfile:
                self.dicc = json.load(jsonfile)
                print("Actualizamos página")

        except KeyError:
            pass

    def handle(self):

        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        linea = self.rfile.read()
        texto = linea.decode('utf-8')
        registro = texto.split(" ")[0]
        SERVER = self.client_address[0]
        PORT = self.client_address[1]
        email = texto.split(" ")[2]
        caducidad = texto.split(" ")[3].split("\r\n")[1].split(":")[1]
        direccion = ('SERVER: ' + str(SERVER) + ' PORT: ' + str(PORT))
        ahora = time.time() + int(caducidad)
        GMT = '%Y/%m/%d %H:%M:%S'
        tiempocaduc = time.strftime(GMT, time.gmtime(ahora))
        tiempoactual = time.strftime(GMT, time.gmtime(time.time()))
        self.dicc[email] = [SERVER, PORT, tiempocaduc, tiempoactual]
        tiempo = tiempocaduc
        print("Se registra: ", self.dicc, '\r\n')

        if tiempocaduc <= tiempoactual:
            del self.dicc[email]

        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        self.register2json()

        if registro == 'REGISTER':
            self.dicc[email] = [direccion, tiempo]

            if int(caducidad) == 0:
                print("Se va a eliminar: " + email)
                del self.dicc[email]

            for email in self.dicc.copy():
                if tiempoactual >= self.dicc[email][1]:
                    del self.dicc[email]

        if int(caducidad) < 0:
            sys.exit("ERROR: No puede haber caducidad negativa")

        for line in self.rfile:
            print("El cliente nos manda ", line.decode('utf-8'))

        print(direccion)
        print(self.dicc)


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    PORT = int(sys.argv[1])
    if PORT < 1024:
        sys.exit("Error, el puerto es inválido")

    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
